const _ = require("lodash");
const arr = [1, 2, 3, 4, 5];
const sum = _.sum(arr);
console.log(sum); // 15

const data = [1, 2, 3, 4, 5];
const filteredData = _.filter(data, (num) => num % 2 === 0);
console.log(filteredData); // Output: [2, 4]
